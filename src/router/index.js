import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '@/components/HomeVivev'
import Contact from "@/components/Contact";
import License from "@/components/License";
import Uslugi from "@/components/Uslugi";
import Range from "@/components/Range";
import Metro from "@/components/Metro";
import chronos from "@/components/chronos";
import secret from "@/components/secret";
import glass from "@/components/glass";

Vue.use(VueRouter)

const routes = [
  {
    path: '/Contact',
name: 'contact',
component: Contact
  },
  {
    path: '/license',
    name: 'license',
    component: License
  },

  {
    path: '/uslugi',
    name: 'uslugi',
    component: Uslugi
  },

  {
    path: '/biographi',
    name: 'biorgaphi',
    component: Range
  },

  {
    path: '/metro',
    name: 'metro',
    component: Metro
  },

  {
    path: '/secret',
    name: 'secret',
    component: secret
  },

  {
    path: '/chronos',
    name: 'chronos',
    component: chronos
  },

  {
    path: '/glass',
    name: 'glass',
    component: glass
  },







  {
    path: '/',
    name: 'home',
    component: HomeView
  },



]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
